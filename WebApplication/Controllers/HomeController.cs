﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Data;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationContext _context;
        private readonly UserManager<User> _userManager;

        public HomeController(ILogger<HomeController> logger, ApplicationContext context, UserManager<User> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            string userId = _userManager.GetUserId(User);
            List<Exercise> exercises = _context.Exercises.Include("User").Where(p => p.UserId == userId).ToList();
            List<Exercise> data = exercises.Where(p => p.AppointedAt < DateTime.Now && p.Status != Status.InExpired).ToList();
            if (data.Count != 0)
            {
                await Expiredu(data);
            }
            return View(exercises);
        }
        public async Task Expiredu(List<Exercise> exercises)
        {
            IEnumerable<Exercise> enumerable()
            {
                return from Exercise ex in exercises
                       select ex;
            }
            foreach (Exercise ex in enumerable())
            {
                ex.Status = Status.InExpired;
                _context.Update(ex);
            }
            await _context.SaveChangesAsync();
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Id,Text,Status,CreatedAt,PerformAt,AppointedAt")] Exercise exercise)
        {
            if (ModelState.IsValid)
            {
                exercise.Status = Status.InDone;
                var user = await _userManager.GetUserAsync(User);
                exercise.UserId = user.Id;
                _context.Add(exercise);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Take(int id)
        {
            Exercise exercise = await _context.Exercises.FirstOrDefaultAsync(p => p.Id == id);
            if (exercise != null)
            {
                exercise.Status = Status.InProgress;
                _context.Update(exercise);
                await _context.SaveChangesAsync();
            }
            var result = new
            {
                status = exercise.Status
            };
            return Ok(Json(result));
        }
        /* private async Task Expired(string data)
         {
             List<Exercise> exercises = JsonSerializer.Deserialize<List<Exercise>>(data).ToList();
             for(int i = 0; i < exercises.Count; i++)
             {
                 exercises[i].Status = Status.InExpired;
                 _context.Update(exercises[i]);
                 await _context.SaveChangesAsync();
             }
         }*/
        [HttpPost]
        public async Task<IActionResult> Expired(int id)
        {
            Exercise exercise = await _context.Exercises.FirstOrDefaultAsync(p => p.Id == id);
            if (exercise != null)
            {
                exercise.Status = Status.InExpired;
                _context.Update(exercise);
                await _context.SaveChangesAsync();
            }
            var result = new
            {
                status = exercise.Status
            };
            return Ok(Json(result));
        }
        public IActionResult Excel()
        {
            string userId = _userManager.GetUserId(User);
            List<Exercise> exercises = _context.Exercises.Where(p => p.UserId == userId).ToList();
            using (XLWorkbook workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Tasks");
                int currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Текст задачи";
                worksheet.Cell(currentRow, 2).Value = "Дата выполнения задачи";
                worksheet.Cell(currentRow, 3).Value = "Дату назначение задачи";
                worksheet.Cell(currentRow, 4).Value = "Статус";
                foreach (Exercise exercise in exercises)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = exercise.Text;
                    worksheet.Cell(currentRow, 2).Value = exercise.PerformAt;
                    worksheet.Cell(currentRow, 3).Value = exercise.AppointedAt;
                    worksheet.Cell(currentRow, 4).Value = exercise.Status;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "tasks.xlsx");
                }
            }
        }
        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckDay(DateTime? AppointedAt, DateTime? PerformAt)
        {
            if (AppointedAt > DateTime.Now || PerformAt > DateTime.Now)
                return Json(true);
            else
                return Json(false);
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
