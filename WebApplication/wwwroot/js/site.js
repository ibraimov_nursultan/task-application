﻿
function take(id) {
    $.ajax({
        url: "Home/Take",
        type: "POST",
        data: { 'id': id },
        success: function (data) {
            let result = data.value;
            document.getElementById("status-" + id).innerText = result.status;
            document.getElementById("status-" + id).style.cursor = 'unset';
            document.querySelector("#status-" + id).className = 'btn btn-secondary';
        }
    });
}
