﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Exercise
    {
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedAt { get; set; }
        [Required]
        [Remote(action: "CheckDay", controller: "Home", ErrorMessage = "Дата не должна быть текущей или меньше текущей")]
        public DateTime? PerformAt { get; set; }
        [Required]
        [Remote(action: "CheckDay", controller: "Home", ErrorMessage = "Дата не должна быть текущей или меньше текущей")]
        public DateTime? AppointedAt { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public Exercise()
        {
            this.CreatedAt = DateTime.Now;
        }
    }
}
