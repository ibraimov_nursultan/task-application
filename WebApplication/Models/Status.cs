﻿namespace WebApplication.Models
{
    public class Status
    {
        public static string InDone { get { return "Начато"; } }
        public static string InProgress { get { return "Ожидает"; } }
        public static string InExpired { get { return "Просрочено"; } }
    }
}
